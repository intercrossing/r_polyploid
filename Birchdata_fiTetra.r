
# Place all birch files from ' bowtie-sstacks-MatchesTo097Catalog', 
#the url :http://datadryad.com/resource/doi:10.5061/dryad.v5gd2
#in current working directory."Datasetnames.txt" lists out the names of all files


#Read the names of all the files from current working directory  into a table.
datafile <-  read.table("Datasetnames.txt",header=FALSE);
nFileCounter <- 1;
#Create the output empty data frame
SNP_BirchMatches <- data.frame(SampleID = character(),MarkerID=character(),X0 = numeric(),X1=numeric());
df_NewRow <-data.frame();

#Read the files one by one
while(!is.na(datafile[nFileCounter,1]))
{
  aMatches <- read.delim(as.character(datafile[nFileCounter,1]))
    
  # rename the columns with appropriate names
  colnames(aMatches) <- c('SQLID','BatchID','CatalogID','SampleID','StackID','Haplotype','StackDepth')  
                           
  # Exclude those rows where haplotype = consensus
  aMatches <- aMatches[aMatches$Haplotype != "consensus",]
                         
  Counter <- 1;                     
  while(Counter <= length(row.names(aMatches)))
  {
    
    df_FilterAvailableMatches <- aMatches[which(aMatches$StackID == aMatches$StackID[Counter]),];
    
    if(length(row.names(df_FilterAvailableMatches)) >= 2)
    {
      df_FilterAvailableMatches <- df_FilterAvailableMatches[order(-df_FilterAvailableMatches$StackDepth),];
      x0 <- df_FilterAvailableMatches$StackDepth[1];
      x1 <- df_FilterAvailableMatches$StackDepth[2];
    }
    else
    {
      
      x0 <-df_FilterAvailableMatches$StackDepth[1];
      x1 <- 0;      
    }
    df_NewRow <- data.frame(SampleID= df_FilterAvailableMatches$SampleID[1],MarkerID=paste(df_FilterAvailableMatches$BatchID[1],
                 df_FilterAvailableMatches$CatalogID[1],sep=""),X0=x0,X1=x1);  
    
    #append the new rows
    SNP_BirchMatches <- rbind(SNP_BirchMatches,df_NewRow);
    Counter <- Counter + length(row.names(df_FilterAvailableMatches));
  }
  
  nFileCounter <- nFileCounter +1;  
}




