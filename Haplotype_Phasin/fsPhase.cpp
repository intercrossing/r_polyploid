#include <iostream>
#include <string>
#include <sstream>
#include <math.h> 
#include <fstream>
#include <stdlib.h>  
#include <cstring>
#include <vector>
#include <random>
#include "fsPhase.h"

#define Recomb_InitialValue 0.00001
#define Jumpvalues 3
/*
Inorder to be able to use random header file include the option -std=c++0x in compile command.
To use gsl header files install gsl libraries and then include the option 
pkg-config --libs gsl` in compile command. The final command is,
 ** g++ -std=c++0x  fsPhImitate.cpp  -o fspH **
*/

using namespace std;
int main()
{
  cout << "Phasing example" << "\n";  
  EmHMMGenotype ObjHap;   
  return 0;
}

dataGenotypes::dataGenotypes()
{
  cout << "dataGenotypes::dataGenotypes()" << endl;
}
dataGenotypes::~dataGenotypes()
{
      
}

void dataGenotypes::parseInput()
{
  string tempStr;
  char buf[100];
  ifstream myReadFile;
  myReadFile.open("sim.inp");  
  if (myReadFile.is_open()) 
  {
     myReadFile >> buf;
     nIndividuals = strtol(buf, NULL,10) ;
     myReadFile >> buf;
     nMarkers = strtol(buf, NULL,10) ; 
     myReadFile >> buf;
     while (!myReadFile.eof()) 
     {
      //read physical distances b/w markers      
       if(strcmp(buf,"P")==0 )
       {
         int iTemp1,iTemp2;
         myReadFile >> buf;
         while(1)
         {
           iTemp1 = strtol(buf, NULL,10);
           myReadFile >> buf;
           if(strcmp(buf,"#1")==0)
           {
            break;
           }
           iTemp2 = strtol(buf, NULL,10);          
           m_physicalDistances.push_back(iTemp2-iTemp1);  
          }
        }
         //read the genotypes of the individuals
        if(strcmp(buf,"#1")==0 )
        {            
            while(!myReadFile.eof())
            {
              for(int iCount=0;iCount < nIndividuals; iCount++)
              {     
                vector<int> geno;           
                for(int iCount=0;iCount < nMarkers; iCount++)           
                {
                  myReadFile >> buf;
                  geno.push_back(strtol(buf, NULL,10));
                }
                m_genotypes.push_back(geno);
                myReadFile >> buf;
              }
          }
     }
     }
  }
  myReadFile.close();
}
void dataGenotypes::print_variables()
{
  cout << "No indi: " << nIndividuals << endl << "No markers: " << nMarkers <<endl;
  cout << "PhysicalDistances :" << endl;
  for(vector<double>::iterator i=m_physicalDistances.begin();i != m_physicalDistances.end();i++) 
  {
    cout << "D: " << (*i) << "\t";
  }
  cout << endl << "genotypes :" << endl;
  for(vector<vector<int> >::iterator i =m_genotypes.begin();i != m_genotypes.end();i++ )
  {
    for(vector<int>::iterator j = (*i).begin();j != (*i).end();j++)
    {
      cout << " " << (*j);
    }
    cout << endl;
  }
}


haplotypes::haplotypes()
{
  cout << "haplotypes::haplotypes()" << endl;
  m_input.parseInput();  
  //m_input.print_variables();
}
haplotypes::~haplotypes()
{
  
}
void haplotypes::printParam()
{
  cout << "Recombination values:" << endl;
  for(int iCount =0;iCount < nMarkers-1 ;iCount++)
  {
    cout << m_recombinations[iCount] << endl;
  }
  
  cout << "Theta values;" << endl;
  vector<double>  temp;
  for(int iCount=0;iCount < nMarkers; iCount++)
  {
     temp = m_theta[iCount] ;
     for(vector<double>::iterator jCount=temp.begin(); jCount != temp.end() ;jCount++)
     {
        cout << *jCount << " ";
     }
     cout << endl;
  }
  
  cout << "Alpha values;" << endl;
  for(int iCount=0;iCount < nMarkers; iCount++)
  {
      temp = m_alpha[iCount] ;
      for(vector<double>::iterator jCount=temp.begin(); jCount != temp.end() ;jCount++)
      {
        cout << *jCount << " ";
      }
      cout << endl;
  }
}
void haplotypes::initialiseParam()
{
  //initialise clusters
  nClusters = 3;
  //initialise recombination vector   
  for(int iCount =0;iCount < nMarkers-1 ;iCount++)
  {
    m_recombinations.push_back(Recomb_InitialValue);
  }
  //initialise theta matrix
 
  std::default_random_engine generator;
  std::uniform_real_distribution<double> distribution(0.01,0.99);
  for(int iCount=0;iCount < nMarkers; iCount++)
  {
      vector<double> thetaTemp;
      for(int jCount=0; jCount < nClusters ;jCount++)
      {
	      thetaTemp.push_back(distribution(generator));
      }
      m_theta.push_back(thetaTemp);
   }
      
   //initialse cluster frequencies (alpha) 
 
  //LATER : assign initial values from dirichlet distribution
  for(int iCount=0;iCount < nMarkers; iCount++)
  {  
    vector<double> alphaTemp;   
    for(int jCount=0;jCount< nClusters;jCount++)
    {
	    alphaTemp.push_back(1.0/nClusters);  
    } 
    m_alpha.push_back(alphaTemp);
  }   
 
}
void haplotypes::computeTransProbBetClst()
{
  double dTempJumpprob=0;
  double dTemp;
 
  std::ostringstream marker,to,from;
  string states;
  vector <double> alphaCurMarker;
  for(int jCount=0; jCount < nMarkers-1; jCount++)
  { 
      map<string,double > markerData;
      alphaCurMarker = m_alpha[jCount];
      for(int kCount=0; kCount < nClusters; kCount++)
      { 
        for(int lCount=0; lCount < nClusters; lCount++)
        {    
            dTemp = exp(-1*m_recombinations[jCount]*m_input.m_physicalDistances[jCount]);
            dTempJumpprob = (1-dTemp)*alphaCurMarker[lCount];        
            if( kCount == lCount)
            {
               dTempJumpprob = dTempJumpprob + dTemp;
            }
            marker << (jCount);
            to << (lCount);
            from << (kCount);
            states = marker.str() +"," + from.str()+  "->"+to.str();
            marker.str("");
            to.str("");
            from.str("");
            markerData[states] = dTempJumpprob;             
        }  
      } 
      m_transProbBetClst.push_back(markerData);      
  }   
  /*std::map<string,double> x;
  for(size_t i= 0;i < m_transProbBetClst.size();i++) 
  {
    x = m_transProbBetClst[i];
    for(std::map<string,double>::iterator j= x.begin();j!=x.end();j++) 
    {
      cout <<  (*j).first << " : " << (*j).second << endl;
    }
  }*/
}


void haplotypes::computeTransProbBetClstPairs()
{
  std::ostringstream marker,to1,from1,to2,from2;
  string states;
  double dtempprob =0;
  map<string,double > hapData;
  for(int iCount=0;iCount<nMarkers-1;iCount++)
  {
    hapData = m_transProbBetClst[iCount];
    map<string,double> markerData;
    for(int jCount=0; jCount < nClusters; jCount++)
    {
      for(int kCount=jCount; kCount < nClusters; kCount++) //{jCount,kCount}from pair of clusters
      {
        for(int lCount=0; lCount < nClusters; lCount++)
        {
          for(int mCount=lCount; mCount < nClusters; mCount++)//{lCount,mCount}from pair of clusters
          {
            marker.str("");
            to1.str("");
            from1.str("");
            to2.str("");
            from2.str("");
            marker << (iCount);
            from1 << (jCount);
            from2 << (kCount);
            to1 << (lCount);
            to2 << (mCount);
            states = marker.str() +"," + "{" + from1.str()+ "," 
                     +from2.str()+ "}" + "->" + "{"
                     +to1.str()+ "," + to2.str() +"}";          
            
            dtempprob = hapData[marker.str() +"," + from1.str()+  "->"+to1.str()] *
                        hapData[marker.str() +"," + from2.str()+  "->"+to2.str()];
            if(jCount !=kCount && lCount!=mCount)
            dtempprob = dtempprob +(hapData[marker.str() +"," + from1.str()+  "->"+to2.str()] *
                        hapData[marker.str() + "," + from2.str()+  "->" + to1.str()]);
            
            markerData[states] = dtempprob;
            
          }
        }
      }
    }
    m_transProbBetClstPairs.push_back(markerData);
  }
  /*  std::map<string,double> x;
  for(size_t i= 0;i < m_transProbBetClstPairs.size();i++) 
  {
    x = m_transProbBetClstPairs[i];
    for(std::map<string,double>::iterator j= x.begin();j!=x.end();j++) 
    {
      cout <<  (*j).first << " : " << (*j).second << endl;
    }
    } */
}
EmHMMGenotype::EmHMMGenotype()
{
  cout << "EmHMMGenotype::EmHMMGenotype()" << endl;
  m_hapData.initialiseParam();
  //m_hapData.printParam();
  runEM();
  //m_hapData.computeTransProbBetClst();
  //m_hapData.computeTransProbBetClstPairs();
  
}
EmHMMGenotype::~EmHMMGenotype()
{
  
}

void EmHMMGenotype::runEM()
{
  int i = 0;
  map< int,vector<double> > jumpProbUpdated;
  vector<vector<map<string,double > > > genoGivenClstUpdated;
  vector< vector<map<string,double> > > FwdProbsUpdated;
  vector< vector<map<string,double> > > BckwdProbsUpdated;
  vector<vector<map<string,double > > > clstGivenGenoUpdated;
  vector<vector<double> > alphaUpdated;
  vector<vector<double> > thetaUpdated;
  vector<double> recombinationsUpdated;
  //do
  {
    computeJumpProb();
    computeGenoGivenClst();
    computeFwdProb();
    computeBckwdProb();
    computeClstGivenGeno();
    computeExpJumpGivenGeno();
    //computeThetaUpdated(thetaUpdated);
    //computeAlphaUpdated(alphaUpdated);
    //computeRecombinationUpdated(recombinationsUpdated);
    
    //m_hapData.m_recombinations = recombinationsUpdated;
    //m_hapData.m_theta = thetaUpdated;
    //m_hapData.m_alpha = alphaUpdated;
    
    i++;
  }//while(i<3);
}
void EmHMMGenotype::computeJumpProb()
{
 cout << "EmHMMGenotype::computeJumpProb()" << endl;
 double jumpProb;
  for(int jCount=0; jCount < nMarkers-1; jCount++)
  {
    vector<double>  markerdata;  
   
    jumpProb = exp(-1*m_hapData.m_recombinations[jCount]*
                  m_hapData.m_input.m_physicalDistances[jCount]);

    markerdata.push_back(jumpProb*jumpProb); //j=0
    markerdata.push_back(2*jumpProb*(1-jumpProb));//j=1
    markerdata.push_back((1-jumpProb)*(1-jumpProb));//j=2
    
    m_jumpProb[jCount] = markerdata;  
   /*cout << "JumpProb" << jCount << " :" << markerdata[0]<<" " << markerdata[1]<<" "
   << markerdata[2]<< endl;*/
  } 
}
void EmHMMGenotype::computeGenoGivenClst()
{
  cout << "EmHMMGenotype::computeGenoGivenClst()" << endl;
  std::ostringstream marker,to,from;
  string states;
  double dtempGenovalue,dtempThetak1,dtempThetak2;
  int genotype;
  vector<int> genoCurInd;
  vector<double> thetaCurMarker;
  for(int iCount=0; iCount < nIndividuals; iCount++)
  {
    vector<map<string,double> >  individualData;
    genoCurInd = m_hapData.m_input.m_genotypes[iCount];
    for(int jCount=0; jCount < nMarkers; jCount++)
    {
      map<string,double> markerData;
      genotype = genoCurInd[jCount];
      thetaCurMarker = m_hapData.m_theta[jCount];
      for(int kCount=0;kCount<nClusters;kCount++)
      {
        for(int lCount=kCount;lCount<nClusters;lCount++)
	      {
            marker << (jCount);
            to << (lCount);
            from << (kCount);
            states = marker.str() +"," + from.str()+  ","+to.str();
            marker.str("");
            to.str("");
            from.str("");
            dtempThetak1 =  thetaCurMarker[kCount];
            dtempThetak2 =  thetaCurMarker[lCount];
          
            if(genotype==0)
	          {
	             dtempGenovalue =(1-dtempThetak1)*(1-dtempThetak2);
            }
            else if(genotype==1)
	          {
              dtempGenovalue =((1-dtempThetak1)*dtempThetak2)+((1-dtempThetak2)*dtempThetak1);
            }
            else
	          {
               dtempGenovalue =(dtempThetak1)*(dtempThetak2);
            } //LATER ERROR HANDLING
            markerData[states] = dtempGenovalue; 
            /*if(iCount == nIndividuals-1)
            cout << "GenoGivenClst :" << states << " :" << dtempGenovalue<< endl;*/
        }        
      }
      individualData.push_back(markerData);
    }
    m_genoGivenClst.push_back(individualData);
  }
}
void EmHMMGenotype::computeFwdProb()
{
 
  std::ostringstream marker,to,from,prevMarker;
  string states,prevStates;
  map<string,double> genotypesData; 
  vector<map<string,double > >  genoCurInd;
  double dTempFwdProb; 
  vector<double> AlphaCurMarker;
  for(int indCnt = 0; indCnt < nIndividuals; indCnt++)
  {
    genoCurInd = m_genoGivenClst[indCnt];
    genotypesData = genoCurInd[0];
    AlphaCurMarker = m_hapData.m_alpha[0];
    map<string,double> markerData;
    vector<map<string,double> > indData;
    for(int Clstk1=0;Clstk1<nClusters;Clstk1++)
    {
      for(int Clstk2=Clstk1;Clstk2<nClusters;Clstk2++)
      {
          marker << 0;
          to << (Clstk2);
          from << (Clstk1);
          states = marker.str() + "," + from.str()+  "," + to.str();
          marker.str("");
          to.str("");
          from.str("");
          dTempFwdProb = genotypesData[states]*AlphaCurMarker[Clstk1]*AlphaCurMarker[Clstk2];
          markerData[states]=dTempFwdProb;
      }
    }
    indData.push_back(markerData);
    m_FwdProbs.push_back(indData);
  }
      
  double term1,term2,term3,term21,term22;
  vector<double> jumpProbPrevMarker;
  vector<map<string,double> > FwdProbCurInd;
  map<string,double> FwdProbPrevMarker;
  
  for(int indCnt=0; indCnt < nIndividuals;indCnt++)
  {
    genoCurInd = m_genoGivenClst[indCnt]; //
    FwdProbCurInd = m_FwdProbs[indCnt];//
    
    for(int markCnt=1;markCnt < nMarkers ;markCnt++)
    {
      
      genotypesData =  genoCurInd[markCnt];//
      
      jumpProbPrevMarker = m_jumpProb[(markCnt-1)];//
      
      AlphaCurMarker = m_hapData.m_alpha[markCnt];//
      
      FwdProbPrevMarker = m_FwdProbs[indCnt][markCnt-1];//
      
      map<string,double> markerData;
      
      for(int Clstk1=0;Clstk1 < nClusters; Clstk1++)
      {
          
       for(int Clstk2 = Clstk1;Clstk2 < nClusters;Clstk2++)
       {
	       marker << markCnt;
         prevMarker << (markCnt-1);
         to << Clstk2;
         from << Clstk1;
         states = marker.str() + "," + from.str()+  "," + to.str();
         prevStates = prevMarker.str() + "," + from.str() +  "," + to.str() ;
         
         term1 = jumpProbPrevMarker[0]*FwdProbPrevMarker[prevStates];//
         term21=term22=0;
         std::ostringstream tempStr;
         string tempS21,tempS22;
         for(int iCount=0; iCount < nClusters; iCount++)
	       {
           tempStr << (iCount);
           if(iCount < Clstk2)
           {
               tempS21 =prevMarker.str()+ ","+tempStr.str()+","+to.str();
           }
           else
           {
             tempS21 =prevMarker.str()+ ","+to.str()+","+tempStr.str();
           }
           if(iCount < Clstk1)
           {
               tempS22 =prevMarker.str()+ ","+tempStr.str()+","+from.str();
           }
           else
           {
             tempS22 =prevMarker.str()+ ","+from.str()+","+tempStr.str();
           }
           term21 = term21 + FwdProbPrevMarker[tempS21];//
          
           term22 = term22 + FwdProbPrevMarker[tempS22]; //
           
           tempStr.str("");          
         }         
         term21 = term21*AlphaCurMarker[Clstk1] ;
         term22 = term22*AlphaCurMarker[Clstk2] ;
         term2 = (0.5)*jumpProbPrevMarker[1]*(term21+term22);
         
         term3=0;
         std::ostringstream tempStr1,tempStr2;
         string tempS3;
         for(int iCount=0;iCount< nClusters;iCount++)
         { 
           tempStr1 << iCount;
           
           for(int jCount=iCount;jCount< nClusters;jCount++)
           {
             tempStr2 << jCount;
             if(jCount > iCount)
             {
                tempS3 = prevMarker.str()+ "," 
                   +tempStr2.str()+ ","+tempStr1.str();
             }
             else
             {
                tempS3 = prevMarker.str()+ "," 
                   +tempStr1.str()+ ","+tempStr2.str();
             }
             term3=term3 +FwdProbPrevMarker[tempS3];//
             
             tempStr2.str("");
           }
           tempStr1.str("");
         }
         term3 = term3*AlphaCurMarker[Clstk1]*AlphaCurMarker[Clstk2]*
                 jumpProbPrevMarker[2];
        
         dTempFwdProb = genotypesData[states]*(term1+term2+term3);
         markerData[states] = dTempFwdProb;
         prevMarker.str("");
         marker.str("");
         to.str("");
         from.str("");
       }
      }
     m_FwdProbs[indCnt].push_back(markerData);
     
    }
  }
  //cout<< "CHK FwdProb" << endl;
  /*for(int indCnt=0;indCnt<2;indCnt++)
    {
      for(int MrkCnt=0;MrkCnt<nMarkers;MrkCnt++)
      {
        marker << MrkCnt;
        for(int Clstk1=0;Clstk1 < nClusters; Clstk1++)
        {
          from << Clstk1;
        for(int Clstk2 = Clstk1;Clstk2 < nClusters;Clstk2++)
        {
          to << Clstk2;
          states = marker.str()+","+from.str()+","+to.str() ;
          cout << "computeFwdProb() :"<< states << " :" << m_FwdProbs[indCnt][MrkCnt][states] <<endl;
          to.str("");
        }
        from.str("");
        } 
        marker.str("") ;
      }
      
    }*/
  cout << "EmHMMGenotype::computeFwdProb() end" << endl;
}
void EmHMMGenotype::computeBckwdProb()
{
  cout << "EmHMMGenotype::computeBckwdProb()" << endl;
  std::ostringstream marker,to,from,nextMarker;
  string states;
      
  double dTempFwdProb; 
  marker << nMarkers-1;
  for(int indCnt = 0; indCnt < nIndividuals; indCnt++)
  {
    map<string,double> markerData;
    vector<map<string,double> > indData;
    for(int Clstk1=0;Clstk1<nClusters;Clstk1++)
    {
      for(int Clstk2=Clstk1;Clstk2<nClusters;Clstk2++)
      {
          to << (Clstk2);
          from << (Clstk1);
          states = marker.str() + "," + from.str()+  "," + to.str();          
          to.str("");
          from.str("");
          markerData[states]=1.0;
      }
    }
    indData.push_back(markerData);
    m_BckwdProbs.push_back(indData);
  }
  marker.str("");

  double term1,term2,term3,term21,term22;
  vector<map<string,double > >  genoCurInd;
  map<string,double> genotypesData;
  int nextPos;
  vector<double> jumpProbNxtMarker;
  vector<map<string,double> > BckwrdProbCurInd;
  map<string,double> BckwrdProbNxtMarker;
  vector<double> AlphaNxtMarker;
  for(int indCnt=0;indCnt<nIndividuals;indCnt++)
  {
   
    BckwrdProbCurInd = m_BckwdProbs[indCnt];
    genoCurInd = m_genoGivenClst[indCnt];
    for(int markCnt=nMarkers-2;markCnt >= 0;markCnt--)
    {
      genotypesData = genoCurInd[(markCnt+1)];
      jumpProbNxtMarker = m_jumpProb[markCnt];
      map<string,double> markerData;
      AlphaNxtMarker = m_hapData.m_alpha[(markCnt+1)];
      
      BckwrdProbNxtMarker = m_BckwdProbs[indCnt][0];
      
      
      for(int Clstk1=0;Clstk1 < nClusters; Clstk1++)
      {
        for(int Clstk2 = Clstk1;Clstk2 < nClusters;Clstk2++)
        {
         marker << markCnt;
         nextMarker << (markCnt+1);
         to << Clstk2;
         from << Clstk1;
         states = marker.str() + "," + from.str()+  "," + to.str();
        
         std::ostringstream tempStr;
        
         term1 = jumpProbNxtMarker[0]*genotypesData[nextMarker.str()+
                 "," + from.str()+  "," + to.str()]*BckwrdProbNxtMarker[nextMarker.str() 
                + "," + from.str() +  "," + to.str()];
         
         term21=term22=0;
         
        
        for(int iCount=0;iCount < nClusters; iCount++)
         {
           tempStr << (iCount);
           term21 = term21 + (genotypesData[nextMarker.str()+
                 "," + from.str()+  "," + tempStr.str()]*BckwrdProbNxtMarker[nextMarker.str()+ "," 
                   +from.str()+tempStr.str()]*AlphaNxtMarker[iCount]);
           term22 = term22 +(genotypesData[nextMarker.str()+ "," + tempStr.str()+  "," +
                    to.str()]*BckwrdProbNxtMarker[nextMarker.str()+ "," 
                    +tempStr.str()+ ","+to.str()]*AlphaNxtMarker[iCount]); 
           tempStr.str("");  
         }
         
         term2= jumpProbNxtMarker[1]*(term21+term22);
          
         std::ostringstream tempStr1,tempStr2;
         for(int iCount=0;iCount< nClusters;iCount++)
         { 
           tempStr1 << iCount;
           for(int jCount=iCount;jCount< nClusters;jCount++)
           {
             tempStr2 << jCount;
             term3=term3 + (genotypesData[nextMarker.str()+ "," +tempStr1.str()+  "," + tempStr2.str()]
                           *BckwrdProbNxtMarker[nextMarker.str()+ "," +tempStr1.str()+ ","+tempStr2.str()]
                           *AlphaNxtMarker[iCount] *AlphaNxtMarker[jCount]);
             tempStr2.str("");
           }
           tempStr1.str("");
         }
         term3 = term3*jumpProbNxtMarker[2];         
         
         markerData[states] = term1+term2+term3;         

         nextMarker.str("");
         marker.str("");
         to.str("");
         from.str("");
        }
      }
       //1.we insert markerdata at the beginning of vector.
       m_BckwdProbs[indCnt].insert(m_BckwdProbs[indCnt].begin(),markerData);
    }
  }
  /*cout<< "CHK BckwrdProb" << endl;
  for(int indCnt=0;indCnt<nIndividuals;indCnt++)
    {
      for(int MrkCnt=0;MrkCnt<nMarkers;MrkCnt++)
      {
        marker << MrkCnt;
        for(int Clstk1=0;Clstk1 < nClusters; Clstk1++)
        {
          from << Clstk1;
        for(int Clstk2 = Clstk1;Clstk2 < nClusters;Clstk2++)
        {
          to << Clstk2;
          states = marker.str()+","+from.str()+","+to.str() ;
          cout << "computeBckwdProb() :"<< states << " :" << m_BckwdProbs[indCnt][MrkCnt][states] <<endl;
          to.str("");
        }
        from.str("");
        } 
        marker.str("") ;
      }
      
    }*/
  cout << "EmHMMGenotype::computeBckwdProb() end" << endl;
}

void EmHMMGenotype::computeClstGivenGeno()
{
  cout << "EmHMMGenotype::computeClstGivenGeno()" << endl;
  std::ostringstream marker,to,from;
  string states;
  vector<map<string,double> > FwdProbCurInd;
  vector<map<string,double> > BckwrdProbCurInd;
  map<string,double> FwdProbs;
  map<string,double> BckwrdProbs;
  double dtempvalue;
  
  for(int indCount=0; indCount < nIndividuals; indCount++)
  {
    vector<map<string,double> > individualData;
    
    for(int markCnt=0; markCnt < nMarkers; markCnt++)
    {
       map<string,double> markerData;
       FwdProbs = m_FwdProbs[indCount][markCnt];
       BckwrdProbs = m_BckwdProbs[indCount][markCnt];
       
       for(int kCount=0;kCount<nClusters;kCount++)
       {
         for(int lCount=kCount;lCount<nClusters;lCount++)
         {            
            marker << (markCnt);
            to << (lCount);
            from << (kCount);
            states = marker.str() +"," + from.str()+  ","+to.str();
            marker.str("");
            to.str("");
            from.str("");   
            
            dtempvalue =FwdProbs[states]*BckwrdProbs[states];
            
            if(kCount != lCount)
            {
              dtempvalue = dtempvalue * 2;
            }
            
            markerData[states] = dtempvalue; 
            
        }        
      }
      
      individualData.push_back(markerData);
    }
    m_clstGivenGeno.push_back(individualData); 
  }
  /*cout<< "CHK computeClstGivenGeno " << endl;
  for(int indCnt=0;indCnt<nIndividuals;indCnt++)
  {
      for(int MrkCnt=0;MrkCnt<nMarkers;MrkCnt++)
      {
        marker << MrkCnt;
        for(int Clstk1=0;Clstk1 < nClusters; Clstk1++)
        {
          from << Clstk1;
        for(int Clstk2 = Clstk1;Clstk2 < nClusters;Clstk2++)
        {
          to << Clstk2;
          states = marker.str()+","+from.str()+","+to.str() ;
          cout << "computeClstGivenGeno() :"<< states << " :" << m_clstGivenGeno[indCnt][MrkCnt][states] <<endl;
          to.str("");
        }
        from.str("");
        } 
        marker.str("") ;
      }
   }*/
  cout << "EmHMMGenotype::computeClstGivenGeno() END" << endl;
}

void EmHMMGenotype::computeExpJumpGivenGeno(void)
{
  cout << "EmHMMGenotype::computeExpJumpGivenGeno()" << endl;
  std::ostringstream marker,nextMarker,to,from;
  string states;
  vector<double> AlphaCurMarker;
  vector<double>  JumpprobCurmarker; 
  double dTempEvalue,dTempterm1,dTempterm2,dTempterm3,dTempterm4;
  vector<map<string,double> > FwdProbCurInd;
  map<string,double> FwdProbsCurMarker;  
  vector<map<string,double> > BckwrdProbCurInd;
  map<string,double> BckwrdProbsCurMarker;
  vector<map<string,double> >  GenoGivenClstCurInd;
  map<string,double> GenoGivenClstCurMarker;
  
  for(int indCount=0; indCount < nIndividuals; indCount++)
  {
    vector<vector<double> > individualData;
    GenoGivenClstCurInd = m_genoGivenClst[indCount];
    FwdProbCurInd = m_FwdProbs[indCount];
    BckwrdProbCurInd = m_BckwdProbs[indCount] ; 
    
    for(int markCnt=0; markCnt < nMarkers-1; markCnt++)
    {
      marker << markCnt;
      nextMarker << markCnt+1;
      vector<double> markerData;
      geno = genoCurInd[markCnt];
      JumpprobCurmarker = m_jumpProb[markCnt];
      AlphaCurMarker = m_hapData.m_alpha[markCnt];
      BckwrdProbsCurMarker = BckwrdProbCurInd[markCnt +1];    
      GenoGivenClstCurMarker = GenoGivenClstCurInd[markCnt]; 
      FwdProbsCurMarker = FwdProbCurInd[markCnt];
      
      for(int kCount=0;kCount<nClusters;kCount++)
      {
        
         dTempterm1 = AlphaCurMarker[kCount]/geno; 
         for(int k=0;k<nClusters;k++)
         {
            from << (k);  
            dTempterm2=0;
            for(int l=k;l<nClusters;l++)
            {
              to << (l);
              states = marker.str() +"," + from.str()+  ","+to.str();                
              dTempterm2 = dTempterm2 + FwdProbsCurMarker[states];
              to.str("");
            }  
            from.str(""); 
            dTempterm2 = dTempterm2*JumpprobCurmarker[1]; 
            dTempterm3 = 2*JumpprobCurmarker[2]*geno *AlphaCurMarker[k];
            from << (kCount);
            to << (k);
            states = nextMarker.str() + "," + from.str()+  "," +to.str();  
            to.str("");
            from.str("");
            
            dTempterm4 =  GenoGivenClstCurMarker[states]*BckwrdProbsCurMarker[states];
           
            dTempEvalue = dTempterm1*(dTempterm2+dTempterm3)*dTempterm4 ;           
            
        }
        markerData.push_back(dTempEvalue);
      }            
      individualData.push_back(markerData);
      marker.str("");
      nextMarker.str("");
    }
    m_EvjumpGivenGeno.push_back(individualData); 
  }
  
cout<< "CHK computeExpJumpGivenGeno" << endl;
for(int indCnt=0;indCnt<nIndividuals;indCnt++)
{
  for(int MrkCnt=0;MrkCnt<nMarkers;MrkCnt++)
  {   
      for(int Clstk1=0;Clstk1 < nClusters; Clstk1++)
      {
        cout << "computeExpJumpGivenGeno() :"<< m_EvjumpGivenGeno[indCnt][MrkCnt] <<endl;
      }       
    }      
  }
  cout << "EmHMMGenotype::computeExpJumpGivenGeno() END" << endl;
}
void EmHMMGenotype::computeThetaUpdated(vector<vector<double> > &T)
{
  cout << "EmHMMGenotype::computeThetaUpdated()" << endl;
  double totalProb = 0;
  double tempProb;
  std::ostringstream marker,to,from;
  string states;
  vector<map<string,double> > individualData;
  map<string,double> markerData;
  
  //compute total prob value;
  for(int indCnt= 0; indCnt < nIndividuals; indCnt++)
  {
    individualData =m_clstGivenGeno[indCnt]; 
    for(int mrkCount=0;mrkCount < nMarkers; mrkCount++)
    {
      markerData = individualData[mrkCount];
      marker << (mrkCount);
      for(int kCount=0; kCount < nClusters ;kCount++)
      {
        for(int lCount=kCount; lCount < nClusters ;lCount++)
        {            
            to << (lCount);
            from << (kCount);
            states = marker.str() +"," + from.str()+  ","+to.str();            
            to.str("");
            from.str("");   
            
            tempProb = markerData[states] ;            
            if(kCount == lCount )
            {
              tempProb = tempProb*2;
            }
            
            totalProb = totalProb + tempProb;
        }
      }
      marker.str("");
    }
  }
 
  //compute new theta value
  double dtempTheta=1,dtempTheta1,dtempTheta2;
  vector<int> genoCurInd;
  int geno;
  vector<map<string,double > > clstGivenGenoCurInd;
  map<string,double > clstGivenGenoCurMarker;
  vector<double> thetaOLDcurMarker;
  for(int markCount=0; markCount < nMarkers-1; markCount++)
  {
      vector<double> thetaTemp;
      thetaOLDcurMarker = m_hapData.m_theta[markCount];
      marker << (markCount);
      for(int jCount=0; jCount < nClusters ;jCount++)
      {
         from << (jCount);
         for(int indCnt= 0; indCnt < nIndividuals; indCnt++)
         {
           genoCurInd = m_hapData.m_input.m_genotypes[indCnt];
           geno = genoCurInd[markCount];
           clstGivenGenoCurInd = m_clstGivenGeno[indCnt];
           if(geno != 0)
           {
              clstGivenGenoCurMarker = clstGivenGenoCurInd[markCount];
              for(int kCount=jCount; kCount < nClusters ;kCount++)
              { 
                  to << (kCount);                
                  states = marker.str() +"," + from.str()+  ","+to.str();            
                  to.str("");
              
                  dtempTheta = 1.0;
                  if(geno == 1)
                  {
                    dtempTheta1 = thetaOLDcurMarker[jCount]*(1-thetaOLDcurMarker[kCount]);
                    dtempTheta2 = thetaOLDcurMarker[kCount]*(1-thetaOLDcurMarker[jCount]);
                    dtempTheta = dtempTheta1/(dtempTheta1+dtempTheta2);
                  }
                  dtempTheta = dtempTheta * clstGivenGenoCurMarker[states];
                  if(kCount == jCount)
                  {
                    dtempTheta = dtempTheta*2;
                  }
                  thetaTemp.push_back(dtempTheta);
              }
           }
         }
        from.str("");  
      }
      T.push_back(thetaTemp);
      marker.str("");
   }
   cout << "EmHMMGenotype::computeThetaUpdated() END" << endl;
}
void EmHMMGenotype::computeAlphaUpdated(vector<vector<double> > &T)
{
  cout << "EmHMMGenotype::computeAlphaUpdated()" << endl;
  double dTempalpha,dTempNumerator,dTempDenominator;
  vector<vector<double> > EvJumpCurInd;
  vector<double> EvJumpCurMarker;
  for(int mrkCount=0;mrkCount < nMarkers-1; mrkCount++)  
  {
    vector<double> alphaTemp; 
    dTempDenominator=0;
    for(int indCnt= 0; indCnt < nIndividuals; indCnt++)
    {
        EvJumpCurInd = m_EvjumpGivenGeno[indCnt];
        EvJumpCurMarker = EvJumpCurInd[mrkCount];
        for(int lCount=0;lCount< nClusters ;lCount++)
        {
           dTempDenominator = dTempDenominator + EvJumpCurMarker[lCount];   
        }
    }
    for(int kCount=0; kCount < nClusters ;kCount++)
    {         
      dTempNumerator = 0;
      for(int indCnt= 0; indCnt < nIndividuals; indCnt++)
      {
         EvJumpCurInd = m_EvjumpGivenGeno[indCnt];
         EvJumpCurMarker = EvJumpCurInd[mrkCount];
         dTempNumerator = dTempNumerator+EvJumpCurMarker[kCount];  
      }
      dTempalpha=dTempNumerator/dTempDenominator ;
      alphaTemp.push_back(dTempalpha);
     }
     T.push_back(alphaTemp);
  }
}
void EmHMMGenotype::computeRecombinationUpdated(vector<double> &T)
{  
  cout << "EmHMMGenotype::computeRecombinationUpdated()" << endl;
  double dTempRecomb,dTemp;
  vector<vector<double> > EvJumpCurInd;
  vector<double> EvJumpCurMarker;
  for(int mrkCount=0;mrkCount < nMarkers-1; mrkCount++)  
  {
    dTempRecomb = 0;
    for(int indCnt= 0; indCnt < nIndividuals; indCnt++)
    {
      EvJumpCurInd = m_EvjumpGivenGeno[indCnt];
      EvJumpCurMarker = EvJumpCurInd[mrkCount];
      for(int kCount=0; kCount < nClusters ;kCount++)
      { 
        dTempRecomb = dTempRecomb +EvJumpCurMarker[kCount];
      }
    } 
    dTempRecomb = -1*log((1-dTempRecomb)/2*nIndividuals);
    dTemp = m_hapData.m_input.m_physicalDistances[mrkCount];
    dTempRecomb = dTempRecomb/dTemp ;
    T.push_back(dTempRecomb);
  }

}
























