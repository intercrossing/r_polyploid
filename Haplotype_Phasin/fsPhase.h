#pragma once
#define FSPHASE_INCLUDED

#include <string>
#include <vector>
#include <map>
using namespace std;

int nIndividuals;
int nMarkers;
int nClusters;

class dataGenotypes
{
    public:
    
    vector< vector<int> > m_genotypes; //nIndividuals x nMarkers
    vector<double> m_physicalDistances;
    
    dataGenotypes();
    ~dataGenotypes();
    void parseInput(void);
    void print_variables(void);
};

class haplotypes
{
  public:
   dataGenotypes m_input;
   
   vector<map<string,double > >  m_transProbBetClst; // nMarkers x nClusters x nClusters
   vector<vector<double> > m_alpha; //nClusters x nClusters
   vector<vector<double> > m_theta; // nMarkers x nClusters 
   vector<double> m_recombinations;
   vector<map<string,double > > m_transProbBetClstPairs; // nMarkers x no pairs of Clusters x no pairs of nClusters)
   
   haplotypes();
   ~haplotypes();
   void initialiseParam(void); // initialises v(alpha,theta,r)
   void computeTransProbBetClst(void);
   void computeTransProbBetClstPairs(void);
   void printParam(void);
};

class EmHMMGenotype
{
  public:
  haplotypes m_hapData;
  
  //this is analogous to transition props of a HMM  
  map< int,vector<double> > m_jumpProb; // nMarkers x 3 (J=0,1 and 2)                  
  
  //this is analogous to emission probs of a HMM
  vector<vector<map<string,double > > > m_genoGivenClst;//nIndividuals x nMarkers x no pairs of Clusters

  //fwd probabolities
  vector< vector<map<string,double> > > m_FwdProbs;

 //Bckwd probabolities
  vector< vector<map<string,double> > > m_BckwdProbs;
  
  //an intermediate variable
  vector<vector<map<string,double > > > m_clstGivenGeno;//nIndividuals x nMarkers x no pairs of Clusters
  
  //number of jumps that occur to cluster k,J(imk)
  vector<vector<vector<double > > > m_EvjumpGivenGeno;//nIndividuals x nMarkers x no of Clusters
  
   EmHMMGenotype();
  ~EmHMMGenotype();
  
  void runEM(void);
  void computeJumpProb(void);
  void computeGenoGivenClst(void);
  void computeFwdProb(void);
  void computeBckwdProb(void);
  void computeClstGivenGeno(void);
  void computeExpJumpGivenGeno(void);
  
  void computeThetaUpdated(vector<vector<double> > &T);
  void computeAlphaUpdated(vector<vector<double> > &T);
  void computeRecombinationUpdated(vector<double> &T);
  
};
