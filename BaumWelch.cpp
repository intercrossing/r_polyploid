#include <iostream>
#include <string>
#include <sstream>
#include <math.h> 
#include "Baumwelch.h"

int main()
{
  cout << "Baumwelch  corpus example" << "\n";
  
  init_variables();
  
  HMM_Corpus objHmmCorpus(OP_symbols,states,corpus1,iCorpusValues);
  
  objHmmCorpus.EmBaumWelch();
  
  cout << "End" << "\n";

  return 0;
}
void init_variables(void) 
{
  
  states.push_back("s");
  states.push_back("t");
  
  OP_symbols.push_back("A");
  OP_symbols.push_back("B");
 
  corpus1.push_back("A");
  corpus1.push_back("B");
  corpus1.push_back("B");
  corpus1.push_back("A");  
  corpus1.push_back(",");
  corpus1.push_back("B");
  corpus1.push_back("A");
  corpus1.push_back("B");
  corpus1.push_back(",");
  

  iCorpusValues.push_back(10);
  iCorpusValues.push_back(20); 
  
}



HMM_Corpus::HMM_Corpus(vector<string> sOpSymbols,vector<string>sStates,
vector<string>sOpSeq,vector<int>iCorpusValues)
{
  initVariables(sOpSymbols,sStates,sOpSeq,iCorpusValues);
  print_variables();  
}

void HMM_Corpus::initVariables(vector<string> sOpSymbols,vector<string>sStates,
vector<string>sOpSeq,vector<int>iCorpusValues) 
{
  m_dLikelihood = 0; 
  m_iCounter = 1; 
  for(vector<string>::iterator i=sStates.begin();i!=sStates.end();i++)
  {
    m_vStates.push_back(*i);    
  }
  
  for(vector<string>::iterator i=sOpSymbols.begin();i!=sOpSymbols.end();i++)
  {
    m_vOP_symbols.push_back(*i);
  }
  
  int iCounter=0;
  vector<string> tempseq;
  for(vector<string>::iterator i = sOpSeq.begin(); i != sOpSeq.end(); i++)
  {        
     if((*i) != ",")
     {
        tempseq.push_back(*i);
     }
     else
     {
       Corpus tempCorpus ;
       tempCorpus.m_sOutputString = tempseq;
       tempCorpus.m_iCorpusValue  = iCorpusValues[iCounter];
       tempCorpus.m_dTotalProb = 0;
       m_vOutPutSeqs.push_back(tempCorpus);
       iCounter++;
       tempseq.clear();
     }       
  }
  
  
  m_mStart_probability["s"] = 0.85;
  m_mStart_probability["t"] = 0.15;

  
  m_mTransition_probability["s,s"] = 0.3;
  m_mTransition_probability["s,t"] = 0.7;
  m_mTransition_probability["t,s"] = 0.1;
  m_mTransition_probability["t,t"] = 0.9;
  
  
  m_mEmission_probability["s,A"] = 0.4;
  m_mEmission_probability["s,B"] = 0.6;
  m_mEmission_probability["t,A"] = 0.5;
  m_mEmission_probability["t,B"] = 0.5;

  /*for(vector<string>::iterator i = sStates.begin(); i != sStates.end(); i++)
  {
     m_mStart_probability[*i] =  
  }
  for(vector<string>::iterator Fromstates= sStates.begin(); Fromstates != sStates.end(); Fromstates++)
  {
     for(vector<string>::iterator Tostates = sStates.begin(); Tostates != sStates.end(); Tostates++)
     {
        m_mTransition_probability[*Fromstates+","+*Tostates] =
     }
  }
  for(vector<string>::iterator Fromstates= sStates.begin(); Fromstates != sStates.end(); Fromstates++)
  {
    for(vector<string>::iterator i=sOpSymbols.begin();i!=sOpSymbols.end();i++)
    {
       m_mEmission_probability[*Fromstates+","+*i] = 
    }
  }
*/
    
}
void HMM_Corpus::print_variables(void) 
{
 
  cout << "States:" << endl;
  for(vector<string>::iterator i=m_vStates.begin();i!=m_vStates.end();i++) 
  {
    cout << "S: " << (*i) << endl;
  }
  
  cout << "Op strings:" << endl;
  for(vector<Corpus>::iterator i= m_vOutPutSeqs.begin();i!=m_vOutPutSeqs.end();i++) 
  {
     vector<string> test = (*i).m_sOutputString;
    for(vector<string>::iterator i=test.begin();i!=test.end();i++) 
    {
    	cout << (*i);
    }
    cout << endl;
  }

  cout << endl << "Start probabilities:" << endl;
  for(std::map<string,double>::iterator i=m_mStart_probability.begin();i!=m_mStart_probability.end();i++) 
  {
    cout << "S: " << (*i).first << " : " << (*i).second << endl;
  }  
  
  cout << "Transition probabilities:" << endl;
  for(std::map<string,double>::iterator i= m_mTransition_probability.begin();i!=m_mTransition_probability.end();i++) 
  {
    cout << "S: " << (*i).first << " : " << (*i).second << endl;
  }

  cout << "Emission probabilities:" << endl;
  for(std::map<string,double>::iterator i= m_mEmission_probability.begin();i!=m_mEmission_probability.end();i++) 
  {
    cout << "S: " << (*i).first << " : " << (*i).second << endl;
  }
 
}
void HMM_Corpus::EmBaumWelch(void)
{
  std::map <string,double> start_prob_Updated ;
  std::map <string,double> trans_prob_Updated ;
  std::map <string,double> emiss_prob_Updated;  
  do
  {
    //Estep
     computeForwardProb();        
     computeBackwardProb();     
     computeAvalues();
     computeEvalues();
    
    //M-step
     maximizeStartProb(start_prob_Updated);
     maximizeTransProb(trans_prob_Updated);
     maximizeEmissionProb(emiss_prob_Updated);
   
     //update the model
     m_mStart_probability = start_prob_Updated;
     m_mTransition_probability = trans_prob_Updated;
     m_mEmission_probability = emiss_prob_Updated; 

  }while(!checkConvergence());  
  
  cout << "FINAL values" <<endl;
  cout << endl << "Start probabilities:" << endl;
  for(std::map<string,double>::iterator i=m_mStart_probability.begin();i!=m_mStart_probability.end();i++) 
  {
    cout << "S: " << (*i).first << " : " << (*i).second << endl;
  }  
  
  cout << "Transition probabilities:" << endl;
  for(std::map<string,double>::iterator i= m_mTransition_probability.begin();i!=m_mTransition_probability.end();i++) 
  {
    cout << "S: " << (*i).first << " : " << (*i).second << endl;
  }

  cout << "Emission probabilities:" << endl;
  for(std::map<string,double>::iterator i= m_mEmission_probability.begin();i!=m_mEmission_probability.end();i++) 
  {
    cout << "S: " << (*i).first << " : " << (*i).second << endl;
  }

}
bool HMM_Corpus::checkConvergence()
{
   bool bIsConverging = false;
   double dLikelihood = 0;
   for(vector<Corpus>::iterator i= m_vOutPutSeqs.begin();i!=m_vOutPutSeqs.end();i++) 
   {
    dLikelihood += ((*i).m_iCorpusValue) *log( (*i).m_dTotalProb);
   }
   std::cout.precision(20);
   cout << "Likelihood for HMM" << m_iCounter << ":" << dLikelihood << endl;
   
   
   if(m_dLikelihood == dLikelihood )
   bIsConverging = true;

   m_dLikelihood = dLikelihood;
   m_iCounter++;

   return bIsConverging;
}
void HMM_Corpus::computeForwardProb(void)
{
  vector<string>::iterator OP_Iter;
  std::ostringstream OP_pos_str; //output string stream
  int OP_pos;
  
  string PrevPos;
  vector<string> corpus;
   
  for(vector<Corpus>::iterator OP_SeqIter=m_vOutPutSeqs.begin();OP_SeqIter != m_vOutPutSeqs.end();OP_SeqIter++)
  {
       
      corpus = (*OP_SeqIter).m_sOutputString;
      OP_Iter = corpus.begin();
      OP_pos =1;
      OP_pos_str.str("");
      OP_pos_str << OP_pos;
      for(vector<string>::iterator j=m_vStates.begin();j!=m_vStates.end();j++)
      {               
          (*OP_SeqIter).m_mForwdProb[OP_pos_str.str() +"," +(*j) ] = m_mStart_probability[(*j)]* 
                                                   m_mEmission_probability[(*j)+","+ (*OP_Iter)];          
      }
       
      double alpha_value;
      for(OP_Iter=corpus.begin()+1;OP_Iter!=corpus.end();OP_Iter++)    
      {
         PrevPos = OP_pos_str.str(); 
         OP_pos_str.str("");
         OP_pos++;
         OP_pos_str << OP_pos;  
         for(vector<string>::iterator Fromstate=m_vStates.begin(); Fromstate!=m_vStates.end();Fromstate++)
         {
            alpha_value = 0.0;
            for(vector<string>::iterator Tostate=m_vStates.begin(); Tostate!=m_vStates.end();Tostate++) 
            {
                alpha_value = alpha_value + ((*OP_SeqIter).m_mForwdProb[PrevPos + "," + *Tostate]*
                        m_mTransition_probability[*Tostate+ "," + *Fromstate]*
                        m_mEmission_probability[*Fromstate+ "," + *OP_Iter]); 
          
            }
            (*OP_SeqIter).m_mForwdProb[OP_pos_str.str() +"," + *Fromstate ] = alpha_value;       
         }   
      }
      (*OP_SeqIter).m_dTotalProb = totalProb(corpus.size(),(*OP_SeqIter).m_mForwdProb);
     /*cout<< "CHECK-alpha value:" << endl;
     std::map<string,double> T = (*OP_SeqIter).m_mForwdProb ;
     for(std::map<string,double>::iterator i=T.begin();i!=T.end();i++) 
     {
        cout << "S: " << (*i).first << " : " << (*i).second << endl ;
     }  */   
  }  
}

double HMM_Corpus::totalProb(int size,std::map<string,double> &T)
{
  double total_prob = 0;   
  int pos = 0; 
  std::map<string,double>::iterator i = T.end();  
  i--;
  for(; i != T.begin(); i--) 
  {
    std::istringstream ss(((*i).first).substr(0,1));
    ss >> pos;
    if(pos == size)
    {
      total_prob = total_prob + (*i).second ;     

    }
    if(pos < size)
    break;
  }

 return total_prob;
}
void HMM_Corpus::computeBackwardProb(void)
{
  vector<string>::iterator OP_Iter;
  std::ostringstream OP_pos_str; //output string stream
  int OP_pos;
  
  string PrevPos;
  
  vector<string> corpus;
  for(vector<Corpus>::iterator OP_SeqIter=m_vOutPutSeqs.begin();OP_SeqIter != m_vOutPutSeqs.end();OP_SeqIter++)
  {
      corpus = (*OP_SeqIter).m_sOutputString;
      OP_Iter=corpus.end()-1;
      OP_pos =corpus.size();
      OP_pos_str.str("");
      OP_pos_str << OP_pos;
      for(vector<string>::iterator state=m_vStates.begin(); state!=m_vStates.end();state++) 
      {
        (*OP_SeqIter).m_mBckwdProb[OP_pos_str.str() + "," +*state ] = 1;    
      }

      double beta_value;
      for(OP_Iter=corpus.end()-1;OP_Iter != corpus.begin();OP_Iter--)    
      {
         PrevPos = OP_pos_str.str(); 
         OP_pos_str.str("");
         OP_pos--;
         OP_pos_str << OP_pos; 
         for(vector<string>::iterator Fromstate = m_vStates.begin(); Fromstate!=m_vStates.end(); Fromstate++)
         {
           beta_value = 0.0;
           for(vector<string>::iterator Tostate=m_vStates.begin(); Tostate!=m_vStates.end();Tostate++) 
           {
              beta_value =  beta_value + ((*OP_SeqIter).m_mBckwdProb[PrevPos +"," + *Tostate ]*
                          m_mTransition_probability[*Fromstate + ","+ *Tostate]*
                          m_mEmission_probability[*Tostate + "," + *OP_Iter]); 
           }
           (*OP_SeqIter).m_mBckwdProb[OP_pos_str.str() + "," + *Fromstate] = beta_value;
        }
      }
      /*cout<< "CHECK-beta value:" << endl;
      map<string,double>T =(*OP_SeqIter).m_mBckwdProb ;
      for(std::map<string,double>::iterator i=T.begin();i!=T.end();i++) 
      {
         cout << "S: " << (*i).first << " : " << (*i).second << endl ;
      } */
   }  
}

void HMM_Corpus::computeAvalues(void)
{
  vector<string>::iterator OP_Iter;
  std::ostringstream OP_pos_str; //output string stream 
  double total_prob ;
  int OP_pos_next;
  double gamma_value;
   
  vector<string> corpus;
  for(vector<Corpus>::iterator OP_SeqIter=m_vOutPutSeqs.begin();OP_SeqIter != m_vOutPutSeqs.end();OP_SeqIter++)
  {
     corpus = (*OP_SeqIter).m_sOutputString;
     //get total probability of corpus from alpha values
     total_prob  = (*OP_SeqIter).m_dTotalProb ;
     OP_pos_next = 1;
     OP_pos_str.str("");
     OP_pos_str << OP_pos_next;
     string curPos;
     for(OP_Iter=corpus.begin();OP_Iter!=corpus.end() -1;OP_Iter++)    
     {
        curPos = OP_pos_str.str(); 
        OP_pos_str.str("");
        OP_pos_next++;
        OP_pos_str << OP_pos_next; 
        for(vector<string>::iterator Fromstate=m_vStates.begin(); Fromstate!=m_vStates.end(); Fromstate++)
        {
          for(vector<string>::iterator Tostate=m_vStates.begin(); Tostate != m_vStates.end();Tostate++) 
          {
               gamma_value = (*OP_SeqIter).m_mForwdProb[curPos+ ","+ *Fromstate] * m_mTransition_probability[*Fromstate+","+*Tostate]
                         * m_mEmission_probability[*Tostate +","+ *(OP_Iter+1)] 
                         * (*OP_SeqIter).m_mBckwdProb[OP_pos_str.str() + "," + *Tostate];
          
               (*OP_SeqIter).m_mAtranstions[curPos + "," + *Fromstate + "," + *Tostate] = gamma_value /total_prob;       
          }
        }
     }

    /*cout<< "CHECK-A(gamma) values:" << endl;
     std::map<string,double> T = (*OP_SeqIter).m_mAtranstions ;
     for(std::map<string,double>::iterator i=T.begin();i!=T.end();i++) 
     {
        cout << "S: " << (*i).first << " : " << (*i).second << endl ;
     }*/
  }  
}

void HMM_Corpus::computeEvalues(void)
{
  vector<string>::iterator OP_Iter;
  std::ostringstream OP_pos_str; //output string stream
  int OP_pos;
  double delta_value;
  double total_prob;

  vector<string> corpus;
  for(vector<Corpus>::iterator OP_SeqIter=m_vOutPutSeqs.begin();OP_SeqIter != m_vOutPutSeqs.end();OP_SeqIter++)
  {
     OP_pos =1;
     delta_value =0;
     corpus = (*OP_SeqIter).m_sOutputString;
     //get total probability of corpus from alpha values
     total_prob  = (*OP_SeqIter).m_dTotalProb ;

      for(OP_Iter=corpus.begin();OP_Iter!=corpus.end() ; OP_Iter++) 
      {     
         OP_pos_str.str("");
         OP_pos_str << OP_pos;
         OP_pos++;
         for(vector<string>::iterator Fromstate = m_vStates.begin(); Fromstate != m_vStates.end(); Fromstate++)
         {
           delta_value =  (*OP_SeqIter).m_mForwdProb[OP_pos_str.str()+ "," + *Fromstate]*
                          (*OP_SeqIter).m_mBckwdProb[OP_pos_str.str()+ "," + *Fromstate]; 

           (*OP_SeqIter).m_mEemissions[OP_pos_str.str()+ "," + *Fromstate]= delta_value/ total_prob;        
         }
      }
     /* 
      cout<< "CHECK-E(delta) values:" << endl;
      std::map<string,double> T = (*OP_SeqIter).m_mEemissions ;
      for(std::map<string,double>::iterator i=T.begin();i!=T.end();i++) 
      {
         cout << "S: " << (*i).first << " : " << (*i).second << endl ;
      }
     */
  }
}


void HMM_Corpus::maximizeStartProb(std::map<string,double> &T)
{
  double I=0;
   
  for(vector<string>::iterator Fromstate = m_vStates.begin(); Fromstate != m_vStates.end(); Fromstate++)
  {
     for(vector<Corpus>::iterator OP_SeqIter=m_vOutPutSeqs.begin();OP_SeqIter != m_vOutPutSeqs.end();OP_SeqIter++)
     {
       T[*Fromstate] = ((*OP_SeqIter).m_mEemissions)["1,"+*Fromstate]*((*OP_SeqIter).m_iCorpusValue);
     }
     I = I + T[*Fromstate] ;
  }
  
  for(vector<string>::iterator Fromstate = m_vStates.begin(); Fromstate != m_vStates.end(); Fromstate++)
  {
    T[*Fromstate] = T[*Fromstate]/I ;
  }
  /*cout<< "CHK New start prob:" <<endl;
  for(std::map<string,double>::iterator i=T.begin();i!=T.end();i++) 
  {
    cout << "S: " << (*i).first << " : " << (*i).second << endl;
  } */
}

void HMM_Corpus::maximizeTransProb(std::map<string,double> &T)
{
  double K=0,L=0;    
  vector<string>::iterator OP_Iter;
  std::ostringstream OP_pos_str; //output string stream
  map<string,double> maptempT;
  map<string,double> maptempProb;
  vector<string> corpus;
  for(vector<string>::iterator Fromstate = m_vStates.begin(); Fromstate != m_vStates.end(); Fromstate++)
  {
    for(vector<string>::iterator Tostate = m_vStates.begin(); Tostate != m_vStates.end(); Tostate++) 
    {
      for(vector<Corpus>::iterator OP_SeqIter=m_vOutPutSeqs.begin();OP_SeqIter != m_vOutPutSeqs.end();OP_SeqIter++)
      {
        corpus = (*OP_SeqIter).m_sOutputString;
        for(unsigned int icounter=1;icounter <= corpus.size()-1;icounter++) 
        {     
           OP_pos_str.str("");
           OP_pos_str << icounter;         
           K= K + (*OP_SeqIter).m_mAtranstions[OP_pos_str.str()+","+*Fromstate+","+*Tostate];
        }
        K = K*(*OP_SeqIter).m_iCorpusValue;//value for single OP sequence       
        L = L+ K ;//sum value for all OP sequences
        K=0;
     }
     maptempT[*Fromstate+","+ *Tostate] = L;  
     L=0;   
   }
 }
  double tempProb=0;
  string tempStr ;
  for(vector<string>::iterator Fromstate = m_vStates.begin(); Fromstate != m_vStates.end(); Fromstate++)
  {
    for(map<string,double>::iterator Tostate = maptempT.begin(); Tostate != maptempT.end(); Tostate++) 
    {    
        tempStr = ((*Tostate).first).substr(0,1);
        if(*Fromstate == tempStr)
	{
          tempProb = tempProb + (*Tostate).second;          
	}       
    }
    maptempProb [*Fromstate] = tempProb;
    tempProb=0;
  }
  for(vector<string>::iterator Fromstate = m_vStates.begin(); Fromstate != m_vStates.end(); Fromstate++)
  {
    for(vector<string>::iterator Tostate = m_vStates.begin(); Tostate != m_vStates.end(); Tostate++) 
    {
	T[*Fromstate+","+*Tostate] = maptempT[*Fromstate+","+*Tostate]/maptempProb[*Fromstate];
    }
  }
  /*cout<< "CHK New transition prob: " << endl;
  for(std::map<string,double>::iterator i=T.begin();i!=T.end();i++) 
  {
    cout << (*i).first << " : " << (*i).second << endl;
  }*/
      
}

void HMM_Corpus::maximizeEmissionProb(std::map<string,double> &T)
{
  vector<string>::iterator OP_Iter;
  std::ostringstream OP_pos_str; 
  double K1=0,K2=0;
  map<string,double> maptempT;
  map<string,double> maptempProb;
  vector<string> corpus;
  for(vector<string>::iterator Fromstate = m_vStates.begin(); Fromstate != m_vStates.end(); Fromstate++)  
  {
    for (int icounter=0;icounter< m_vOP_symbols.size();icounter++)
    {
       
       for(vector<Corpus>::iterator OP_SeqIter=m_vOutPutSeqs.begin();OP_SeqIter != m_vOutPutSeqs.end();OP_SeqIter++)
       {
          corpus = (*OP_SeqIter).m_sOutputString;
     	  for(int jcounter=0;jcounter<corpus.size();jcounter++) 
      	  {     
             OP_pos_str.str("");
             if(corpus[jcounter] == m_vOP_symbols[icounter])
             {
                OP_pos_str << jcounter +1;
                K1 = K1 + (*OP_SeqIter).m_mEemissions[OP_pos_str.str()+","+ *Fromstate];   
             }
          }
          K1 = K1*(*OP_SeqIter).m_iCorpusValue;   //value for single OP sequence
          K2 = K2 + K1; //sum value for all OP sequences
          K1=0;
      }
       maptempT[*Fromstate+","+m_vOP_symbols[icounter]] = K2;
       K2=0;
    }    
  }
  double tempProb=0;
  string tempStr ;
  for(vector<string>::iterator Fromstate = m_vStates.begin(); Fromstate != m_vStates.end(); Fromstate++)
  {
     for(map<string,double>::iterator Tostate = maptempT.begin(); Tostate != maptempT.end(); Tostate++) 
     {    
        tempStr = ((*Tostate).first).substr(0,1);
        if(*Fromstate == tempStr)
	{
          tempProb = tempProb + (*Tostate).second;          
	}       
     }
     maptempProb [*Fromstate] = tempProb;
     tempProb=0;
  }
  for(vector<string>::iterator Fromstate = m_vStates.begin(); Fromstate != m_vStates.end(); Fromstate++)
  {
    for(vector<string>::iterator OP_Iter = m_vOP_symbols.begin(); OP_Iter != m_vOP_symbols.end(); OP_Iter++) 
    {
	T[*Fromstate+","+*OP_Iter] = maptempT[*Fromstate+ "," + *OP_Iter]/maptempProb[*Fromstate];
    }
  }
  /*cout<< "CHK New Emission prob: " <<endl;  
  for(std::map<string,double>::iterator i=T.begin();i!=T.end();i++) 
  {
    cout << (*i).first << " : " << (*i).second << endl;
  }  */
  
}
























