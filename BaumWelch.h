#pragma once
#define BAUMWELCH_H_INCLUDED

#include <string>
#include <vector>
#include <map>
using namespace std;
/* This code is implementation of example with two corpus , illustrating Baum welch algorithm detailed in document Baumwelch.pdf.The source link of this document is http://www.indiana.edu/~iulg/moss/hmmcalculations.pdf */

//Input variables


vector<string> states; 
vector<string> OP_symbols;
vector<string> corpus1;
vector<int>iCorpusValues;

void init_variables(void);

template <typename Map>
bool map_compare (Map const &lhs, Map const &rhs) 
{
    
    return lhs.size() == rhs.size()
        && std::equal(lhs.begin(), lhs.end(),
                      rhs.begin());
}

class Corpus
{
 public:
 vector<string> m_sOutputString;
 int m_iCorpusValue;
 double m_dTotalProb;
 std::map <string,double> m_mForwdProb;
 std::map <string,double> m_mBckwdProb;
 std::map <string,double> m_mAtranstions;
 std::map <string,double> m_mEemissions ; 
};

class HMM_Corpus
{
 private: 

 double m_dLikelihood; //this is to check for convergence of the model  
 int m_iCounter; // To count num of iterations before convergence
 vector<string> m_vStates;
 vector<string> m_vOP_symbols;
 vector<Corpus> m_vOutPutSeqs;
 
 std::map<string,double> m_mStart_probability;
 std::map<string,double> m_mTransition_probability;
 std::map<string,double> m_mEmission_probability;
 
 void initVariables(vector<string> sOpSymbols,vector<string>sStates,
 vector<string>sOpSeq,vector<int>iCorpusValues);
 
 void print_variables(void);


 //E-step
 void computeForwardProb();
 double totalProb(int size,std::map<string,double> &T);
 void computeBackwardProb();
 void computeAvalues();
 void computeEvalues();
 //M-step
 void maximizeStartProb(std::map<string,double> &T);
 void maximizeTransProb(std::map<string,double> &T);
 void maximizeEmissionProb(std::map<string,double> &T);
 bool checkConvergence();
 
 public:
 HMM_Corpus(vector<string> sOpSymbols,vector<string>sStates,vector<string>sOpSeq,vector<int>iCorpusValues);
 void EmBaumWelch();
};

